from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .forms import RegistrationForm

# Create your views here.
def register(request):
    user = request.user
    if user.is_authenticated:
        return render(request, 'already_login.html')
    context = {}

    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        data = {}
        if form.is_valid():
            print("berhasil")
            form.save()
            email = form.cleaned_data['email']
            password = form.cleaned_data['password1']
            user = authenticate(email=email, password=password)
            login(request, user)
            data['success'] = True
            return redirect('home:home')
        else:
            print("gagal asw")
            data['error'] = form.errors
            context['registration_form'] = form
            data['success'] = False
    else:
        form = RegistrationForm()
        context['registration_form'] = form

    return render(request, 'register.html', context)
    
def already_login(request):
    return render(request, 'already_login.html')