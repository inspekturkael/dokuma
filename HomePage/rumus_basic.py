def obesitas(param):
    imt = param['bb']/((param['tb']/100)**2)
    obesitas = ""
    if(imt < 18.5) :
        obesitas = "Underweight"
    elif (imt >= 18.5 and imt <= 22.94):
        obesitas = "Normal"
    elif (imt > 22.94 and imt <= 24.95):
        obesitas = "Overweight"
    elif (imt > 24.95 and imt <= 29.95):
        obesitas = "Obesitas I"
    elif (imt > 29.95):
        obesitas = "Obesitas II"

    param['obesitas'] = obesitas

def kalori(param):
    # Berat badan ideal
    bbi = 0
    is_cowo = param['is_cowo']
    tb = param['tb']
    umur = param['umur']
    pekerjaan = param['pekerjaan']
    stres_metabolik = param['stres_metabolik']

    if((is_cowo and tb > 160) or ((not is_cowo) and tb > 150)):
        bbi = 0.9*(tb-100)
    elif((is_cowo and tb <= 160) or ((not is_cowo) and tb <= 150)):
        bbi = (tb-100)

    kalori_basal = 0;
    kalori_harian_final = 0
    if(is_cowo):
        kalori_basal = bbi*30
    else: 
        kalori_basal = bbi*25

    if(umur < 40):
        kalori_harian_final = kalori_basal+(kalori_basal*pekerjaan/10)+(kalori_basal*stres_metabolik/10)
    elif(umur >= 40 and umur <= 59):
        kalori_harian_final = kalori_basal-(kalori_basal*5/100)+(kalori_basal*pekerjaan/10)+(kalori_basal*stres_metabolik/10)
    elif(umur > 59 and umur <= 69):
        kalori_harian_final = kalori_basal-(kalori_basal*10/100)+(kalori_basal*pekerjaan/10)+(kalori_basal*stres_metabolik/10)
    elif(umur > 69):
        kalori_harian_final = kalori_basal-(kalori_basal*20/100)+(kalori_basal*pekerjaan/10)+(kalori_basal*stres_metabolik/10)
        
    makan_pagi = kalori_harian_final*32.5/100
    makan_siang = kalori_harian_final*42.5/100
    makan_malam = kalori_harian_final*25/100

    keperluan_karbo = (65/100)/175
    nasi_pagi = '{:.1f}'.format(makan_pagi*keperluan_karbo)
    nasi_siang = '{:.1f}'.format(makan_siang*keperluan_karbo)
    nasi_malam = '{:.1f}'.format(makan_malam*keperluan_karbo)

    param['bbi'] = bbi
    param['kalori_harian_final'] = kalori_harian_final
    param['nasi_pagi'] = nasi_pagi
    param['nasi_siang'] = nasi_siang
    param['nasi_malam'] = nasi_malam

def rekomendasi_olahraga(param):
    obesitas = param['obesitas']

    rekomendasi_aerobik = ""
    if(obesitas=="Underweight" or obesitas=="Normal"):
        rekomendasi_aerobik = "Sepeda, Jogging, Berenang, Jalan cepat"
    elif(obesitas=="Overweight" or obesitas=="Obesitas I" or obesitas=="Obesitas II"):
        rekomendasi_aerobik = "Sepeda, Berenang, Jalan Cepat"

    rekomendasi_anaerobik = "Angkat beban, Sit up, Push up"

    param['rekomendasi_aerobik'] = rekomendasi_aerobik
    param['rekomendasi_anaerobik'] = rekomendasi_anaerobik

def frekuensi_aktivitas(param):
    umur = param['umur']
    frekuensi_aktivitas = "Aktivitas fisik sesuai usia"
    if(umur < 2):
        pass
    elif(umur>= 2 and umur < 12):
        frekuensi_aktivitas = ""
    elif(umur>=12 and umur<18):
        frekuensi_aktivitas = "Screen time harus kurang dari 2 jam."
    elif(umur>=18 and umur <= 64):
        frekuensi_aktivitas = ["Seminggu 150 menit", "Setiap sesi minimal 10 menit", "Minimal 3x seminggu"]
    elif(umur > 64):
        pass

    param['frekuensi_aktivitas'] = frekuensi_aktivitas