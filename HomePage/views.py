from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from . import rumus_basic
from .render_template import *

# Create your views here.
def index(request):
    return render(request, 'home.html')

@login_required(login_url="login:login")
def mulai(request):
    if request.method == 'POST':
        diagnosis = []
        alergi = []
        umur = int(request.POST.get('umur'))
        tb = int(request.POST.get('tb')) #dalam cm
        bb = int(request.POST.get('bb')) #dalam kg
        is_cowo = (request.POST.get('jenis_kelamin') == "True")
        is_merokok = (request.POST.get('is_merokok') == "True")
        is_hamil = (request.POST.get('is_hamil') == "True")
        is_alkohol = (request.POST.get('is_alkohol') == "True")
        diag = request.POST.getlist('diagnosis')
        pekerjaan = int(request.POST.get('pekerjaan'))
        stres_metabolik = int(request.POST.get('stres'))
        aler = request.POST.getlist('alergi')
        pengobatan = request.POST.get('pengobatan')

        is_dewasa = umur > 17

        for val in diag:
            diagnosis.append(val)
        for val in aler:
            alergi.append(val)

        param = {
            'umur': umur,
            'tb': tb,
            'bb': bb,
            'is_cowo': is_cowo,
            'is_hamil': is_hamil,
            'is_merokok':is_merokok,
            'is_alkohol': is_alkohol,
            'diagnosis': diagnosis,
            'pekerjaan': pekerjaan,
            'stres_metabolik': stres_metabolik,
            'alergi': alergi,
            'pengobatan': pengobatan,
            'is_dewasa': is_dewasa,
        }

        rumus_basic.obesitas(param)

        rumus_basic.kalori(param)

        rumus_basic.rekomendasi_olahraga(param)

        rumus_basic.frekuensi_aktivitas(param)

        if("TB" in diagnosis and "DM" in diagnosis and "HP" in diagnosis):
            return DM_TB_HP(request)

        if("TB" in diagnosis and "DM" in diagnosis):
            return DM_TB(request, param)

        if("TB" in diagnosis and "HP" in diagnosis):
            return TB_HP(request, param)

        if("HP" in diagnosis and "DM" in diagnosis):
            return HP_DM(request, param) 

        if("TB" in diagnosis):
            return TB(request, param)

        if("DM" in diagnosis):
            return DM(request, param)

        if("HP" in diagnosis):
            return HP(request, param)

        if("Anemia" in diagnosis):
            return anemia(request, param)

        if("Influenza" in diagnosis):
            return influenza(request, param)

        if("Faringitis" in diagnosis):
            return faringitis(request, param)

        if("Pneumonia" in diagnosis):
            return pneumonia(request, param)

        if("DDR" in diagnosis):
            return diare_dehidrasi_ringan(request, param)
        
        if("DTR" in diagnosis):
            return diare_tanpa_dehidrasi(request, param)

        if("HeadacheAkut" in diagnosis):
            return headache_akut(request, param)
        
        if("HeadacheKronik" in diagnosis):
            return headache_kronis(request, param)

        if("Migrain" in diagnosis):
            return migrain(request, param)
        
    return render(request, 'diagnosis.html')
