from django.shortcuts import render

from .rumus_dosis import *


def DM_TB_HP(request):
    return render(request, 'penyakit/DM_TB_HP')

def TB_HP(request, param):
    perhitungan_dosis_TB(param)
    return render(request, 'penyakit/TB_HP.html', param)

def DM_TB(request, param):
    perhitungan_dosis_TB(param)
    return render(request, 'penyakit/DM_TB.html', param)

def HP_DM(request, param):
    return render(request, 'penyakit/HP_DM.html', param)

def HP(request, param):
    return render(request, 'penyakit/HP.html', param)

def DM(request, param):
    return render(request, 'penyakit/DM.html', param)

def TB(request, param):
    perhitungan_dosis_TB(param)
    return render(request, 'penyakit/TB.html', param)

def anemia(request, param):
    return render(request, 'penyakit/anemia.html', param)

def faringitis(request, param):
    return render(request, 'penyakit/faringitis.html', param)

def influenza(request, param):
    return render(request, 'penyakit/influenza.html', param)

def pneumonia(request, param):
    return render(request, 'penyakit/pneumonia.html', param)

def migrain(request, param):
    return render(request, 'penyakit/migrain.html', param)

def diare_dehidrasi_ringan(request, param):
    return render(request, 'penyakit/diare_dehidrasi_ringan.html', param)

def diare_tanpa_dehidrasi(request, param):
    return render(request, 'penyakit/diare_tanpa_dehidrasi.html', param)

def headache_akut(request, param):
    return render(request, 'penyakit/headache_akut.html', param)

def headache_kronis(request, param):
    return render(request, 'penyakit/headache_kronis.html', param)