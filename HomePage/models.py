from django.db import models

# Create your models here.
# class IMAGE(models.Model):
#     img = models.URLField()

# class EDUKASI(models.Model):
#     penjelasan = models.TextField()
#     image = models.ForeignKey(to=IMAGE, null=True, on_delete=models.CASCADE)

# class PENYAKIT(models.Model):
#     nama = models.CharField(max_length=255)
#     kompetensi = models.IntegerField()
#     farmakologi = models.TextField()
#     nonfarmakologi_khusus = models.TextField()
#     edukasi = models.ForeignKey(to=EDUKASI, on_delete=models.CASCADE)

class IDENTITAS(models.Model):
    usia = models.SmallIntegerField()
    berat_badan = models.SmallIntegerField()
    tinggi_badan = models.SmallIntegerField()
    is_cowo = models.BooleanField()
    is_hamil = models.BooleanField()
    is_merokok = models.BooleanField()
    aktivitas = models.SmallIntegerField() # {
        # 1 - Istirahat (tirah baring)
        # 2 - Aktivitas ringan (IRT, pegawai kantor, guru)
        # 3 - Aktivitas sedang (pegawai industri ringan, mahasiswa, militer yang sedang tidak perang)
        # 4 - Aktivitas berat (petani, buruh, atlet, militer dalam keadaan latihan)
        # 5 - Aktivitas sangat berat (tukang becak, tukang gali)
    # }
    alergi = models.TextField(blank=True) # null = true
    stres_metabolik = models.SmallIntegerField() # {
        # 0 - tidak ada
        # 1 - trauma ringan
        # 2 - operasi ringan, trauma sedang
        # 3 - sepsis, operasi besar, trauma berat
    #}
    # penyakit = models.ManyToManyField(to=PENYAKIT)


