def perhitungan_dosis_TB(param):
    dosis_KDT_fase_intensif = ""
    dosis_lepasan_intensif = []
    
    umur = param['umur']
    bb = param['bb']

    def kdt_dewasa(bb):
        if(bb > 30 and bb <= 37):
            kdt = "2 tablet"
        elif(bb > 37 and bb <= 54):
            kdt = "3 tablet"
        elif(bb > 54):
            kdt = "4 tablet"

        return kdt 

    if(umur <= 17):
        param['kdt_dewasa'] = (bb > 30)

        if(bb >= 5 and bb <= 7):
            dosis_KDT_fase_intensif = "1 tablet"
        elif(bb > 7 and bb <= 11):
            dosis_KDT_fase_intensif = "2 tablet"
        elif(bb > 11 and bb <= 16):
            dosis_KDT_fase_intensif = "3 tablet"
        elif(bb > 16 and bb <= 22):
            dosis_KDT_fase_intensif = "4 tablet"
        elif(bb > 22 and bb <= 30):
            dosis_KDT_fase_intensif = "5 tablet"
        elif(bb > 30):
            dosis_KDT_fase_intensif = kdt_dewasa(bb)

        #lepasan Rifampisin
        if(bb < 60):
            max_dosis = str(20*bb) if bb < 30 else str(600)
            dosis_lepasan_intensif.append("Rifampisin: " + str(10*bb) + "-" + max_dosis + "mg/hari")
        else:
            dosis_lepasan_intensif.append("Rifampisin: " + "600" + "mg/hari")
        
        #lepasan Isoniazid
        if(bb < 42.87):
            max_dosis = str(15*bb) if bb < 20 else str(300)
            dosis_lepasan_intensif.append("Isoniazid: " + str(7*bb) + "-" + max_dosis + "mg/hari")
        else:
            dosis_lepasan_intensif.append("Isoniazid: " + "300 mg/hari")
        
        #lepasan Pirazinamid
        if(bb < 66.7):
            max_dosis = str(40*bb) if bb < 50 else str(2000)
            dosis_lepasan_intensif.append("Pirazinamid: " + str(30*bb) + "-" + max_dosis + "mg/hari")
        else:
            dosis_lepasan_intensif.append("Pirazinamid: " + "2000 mg/hari")

        #lepasan Etambutol
        if(bb < 100):
            max_dosis = str(25*bb) if bb < 60 else str(1500)
            dosis_lepasan_intensif.append("Etambutol: " + str(30*bb) + "-" + max_dosis + "mg/hari")
        else: 
            dosis_lepasan_intensif.append("Etambutol: " + "2000 mg/hari")

        param["dosis_lepasan_intensif"] = dosis_lepasan_intensif
        param["dosis_KDT_fase_intensif"] = dosis_KDT_fase_intensif
        param['is_dewasa'] = False
    
    if(umur > 17):
        param['kdt_dewasa'] = True
        dosis_KDT_fase_intensif = kdt_dewasa(bb)

        #lepasan Rifampisin
        if(bb < 75):
            max_dosis = str(12*bb) if bb < 50 else str(600)
            dosis_lepasan_intensif.append("Rifampisin: " + str(8*bb) + "-" + max_dosis + "mg/hari")
        else:
            dosis_lepasan_intensif.append("Rifampisin: " + "600" + "mg/hari")
        
        #lepasan Isoniazid
        if(bb < 75):
            max_dosis = str(6*bb) if bb < 50 else str(300)
            dosis_lepasan_intensif.append("Isoniazid: " + str(4*bb) + "-" + max_dosis + "mg/hari")
        else:
            dosis_lepasan_intensif.append("Isoniazid: " + "300 mg/hari")
        
        #lepasan Pirazinamid
        if(bb < 100):
            max_dosis = str(30*bb) if bb < 66.6 else str(2000)
            dosis_lepasan_intensif.append("Pirazinamid: " + str(20*bb) + "-" + max_dosis + "mg/hari")
        else:
            dosis_lepasan_intensif.append("Pirazinamid: " + "2000 mg/hari")

        #lepasan Etambutol
        if(bb < 133.3):
            max_dosis = str(20*bb) if bb < 100 else str(2000)
            dosis_lepasan_intensif.append("Etambutol: " + str(15*bb) + "-" + max_dosis + "mg/hari")
        else: 
            dosis_lepasan_intensif.append("Etambutol: " + "2000 mg/hari")

        param["dosis_lepasan_intensif"] = dosis_lepasan_intensif
        param["dosis_KDT_fase_intensif"] = dosis_KDT_fase_intensif
        param['is_dewasa'] = True

def perhitungan_dosis_DM(param):
    pass

def perhitungan_dosis_diare(param):
    oralit_muntah = param['bb']*5
    oralit_bab = param['bb']*10
    oralit_awal = param['bb']*75

    lt_6 = param['usia'] < 6 and param['skala_usia'] == 'bulan'

    kolera = param['bb']*12.5
    amebiasis = f"3x{param['bb']*10}-{param['bb']*12}"
    
    param['oralit_muntah'] = oralit_muntah
    param['oralit_bab'] = oralit_bab
    param['oralit_awal'] = oralit_awal

    param['lt_6'] = lt_6

    param['kolera'] = kolera
    param['amebiasis'] = amebiasis