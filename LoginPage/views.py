from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from LoginPage.forms import UserAuthenticationForm

# Create your views here.
def login_user(request):
    context = {}
    form = UserAuthenticationForm()
    if request.user.is_authenticated:
        return redirect("home:home")

    if request.method == "POST":
        form = UserAuthenticationForm(request.POST)
        if form.is_valid():
            print("wot")
            email = request.POST['email']
            password = request.POST['password']
            user = authenticate(request, email=email, password=password)
            if user is not None:
                login(request, user)
                return redirect('home:home')
        
    context['login_form'] = form
    return render(request, 'login.html', context)

def logout_user(request):
	logout(request)
	messages.success(request, ("You Were Logged Out!"))
	return redirect('home:home')